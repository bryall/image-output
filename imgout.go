package main

import (
	"bufio"
	"fmt"
	"image"
	"image/png"
	"os"
)

func main() {
	f, _ := os.Open("test.png")
	im, _ := png.Decode(bufio.NewReader(f))
	img := im.(*image.NRGBA)
	s := img.Bounds().Size()
	w := s.X
	h := s.Y
	fmt.Println(w, h)
	iy := 0

	for ; iy < h; iy++ {
		ix := 0
		for ; ix < w; ix++ {
			i := img.PixOffset(ix, iy)
			r := img.Pix[i+0]
			g := img.Pix[i+1]
			b := img.Pix[i+2]

			if r > g && r > b {
				fmt.Printf("\033[31;5;110mo\033[0;00m")
			} else if g > r && g > b {
				fmt.Printf("\033[32;5;110mo\033[0;00m")
			} else if b > g && b > r {
				fmt.Printf("\033[34;5;110mo\033[0;00m")
			} else if b == g && b == r {
				fmt.Printf("o")
			} else {
				fmt.Printf("\033[30mo\033[0;00m")
			}
		}
		fmt.Printf("\n")
	}

}
